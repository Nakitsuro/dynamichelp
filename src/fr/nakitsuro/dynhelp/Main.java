package fr.nakitsuro.dynhelp;

import java.util.ArrayList;

import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.nakitsuro.dynhelp.data.Help;
import lombok.Getter;

public class Main extends JavaPlugin {
	
	public static @Getter
	Main plugin;
	@Getter
	private ArrayList<Help> helps = new ArrayList<Help>();
	@Getter
	Permission perm;
	
	@Override
	public void onEnable() {
		plugin = this;
		Help.init();
		Messages.initMessages(this);
		saveDefaultConfig();
		perm = registerPermission("dynhelp.all");
		this.getCommand("help").setExecutor(new CMDExec());
		this.getCommand("aide").setExecutor(new CMDExec());
	}

	public Permission registerPermission(String permission) {
		Permission perm = new Permission(permission);
		PluginManager pm = this.getServer().getPluginManager();
		if (pm.getPermission(permission) == null) {
			pm.addPermission(perm);
		}
		return perm;
	}

	public void reload() {
		this.getHelps().clear();
		this.reloadConfig();
		Help.init();
	}

}
