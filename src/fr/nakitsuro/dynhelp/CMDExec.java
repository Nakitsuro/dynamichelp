package fr.nakitsuro.dynhelp;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.nakitsuro.dynhelp.data.Help;
import fr.nakitsuro.dynhelp.data.Page;

public class CMDExec implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String arg2, String[] args) {
		if (command.getName().equals("help") || command.getName().equals("aide")) {
			if (sender.hasPermission(Main.getPlugin().getPerm()) || (!(sender instanceof Player))) {
				if (args.length == 0) {
					sender.sendMessage(Messages.getMessage("noargs"));
				} else {
					if (args[0].equalsIgnoreCase("reload")) {
						Main.getPlugin().reload();
						sender.sendMessage(Messages.getMessage("reloaded"));
						return true;
					}
					Help h = Help.getHelpFor(args[0]);
					if (h == null) {
						sender.sendMessage(Messages.getMessage("dontexist"));
						return true;
					}
					int i = 1;
					try {
						i = Integer.parseInt(args[1]);
					} catch (Exception ex) {
					}
					sendHelp(sender, h, i);
				}
			} else {
				Player p = (Player) sender;
				Help h = Help.getHelpFor(p);
				if (h == null) {
					p.sendMessage(Messages.getMessage("noperm"));
					return true;
				}
				int i = 1;
				try {
					i = Integer.parseInt(args[0]);
				} catch (Exception ex) {
				}
				sendHelp(p, h, i);
			}
		}
		return true;
	}

	private void sendHelp(CommandSender sender, Help h, int i) {
		Page p = h.getPage(i);
		if (p == null) p = h.getPage(1);
		for (String s : p.getMessages()) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
		}
	}

}
