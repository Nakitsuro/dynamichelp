package fr.nakitsuro.dynhelp.data;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import fr.nakitsuro.dynhelp.Main;
import lombok.Data;

@Data
public class Help {

	private String flatName;
	private Permission permission;
	private ArrayList<Page> pages;

	public Help(String flatName, Permission permission) {
		this.flatName = flatName;
		this.permission = permission;
		this.pages = new ArrayList<Page>();
	}

	public void addPage(Page p) {
		this.pages.add(p);
	}

	public Page getPage(int i) {
		for (Page page : pages) {
			if (i == page.getPage())
				return page;
		}
		return null;
	}

	public static Help getHelpFor(Player p) {
		for (Help h : Main.getPlugin().getHelps()) {
			if (p.hasPermission(h.getPermission()))
				return h;
		}
		return null;

	}

	public static Help getHelpFor(String p) {
		for (Help h : Main.getPlugin().getHelps()) {
			if (p.equalsIgnoreCase(h.getFlatName()))
				return h;
		}
		return null;

	}

	public static void init() {
		Main pl = Main.getPlugin();
		for (String s : pl.getConfig().getConfigurationSection("help").getKeys(true)) {
			if (!s.contains(".")) {
				String cat = "help." + s;
				String flatName = s;
				String permission = pl.getConfig().getString(cat + ".perm");
				Permission perm = pl.registerPermission(permission);
				Help h = new Help(flatName, perm);
				for (String s1 : pl.getConfig().getConfigurationSection(cat + ".pages").getKeys(true)) {
					if (!s1.contains(".")) {
						int page = 0;
						try {
							page = Integer.parseInt(s1);
						} catch (Exception ex) {
							pl.getLogger().severe("Can't parse the page '" + s1 + "' in the " + cat + " help message.");
							continue;
						}
						Page p = new Page(page, pl.getConfig().getStringList(cat + ".pages." + s1));
						h.addPage(p);
					}
				}
				pl.getHelps().add(h);
			}
		}
	}

}
