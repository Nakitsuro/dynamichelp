package fr.nakitsuro.dynhelp.data;

import java.util.List;

import lombok.Data;

@Data
public class Page {
	
	private int page;
	private List<String> messages;
	
	public Page(int page, List<String> messages) {
		this.page = page;
		this.messages = messages;
	}

}
